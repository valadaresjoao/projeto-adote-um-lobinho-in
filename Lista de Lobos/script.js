const urlAPI = "../lobinhos.json";
var inverter = true


function createLobo(nome, idade, descricao, foto, id) {

    let lobo = document.createElement("div");

    let vazia = document.createElement("vazia")
    let div_lobo = document.createElement("div")
    let img_lobo = document.createElement("img");
    let conteudo_lobo = document.createElement("div");

        let separador_lobo = document.createElement("div");
            let header_lobo = document.createElement("div");
                let nome_lobo  = document.createElement("h3");
                let link_lobo = document.createElement("a")
                let btn_header_lobo = document.createElement("button")
            let idade_lobo = document.createElement("h5");
        
        let descricao_lobo = document.createElement("div");
            let pDescricao = document.createElement("p");
    
    lobo.classList.add("lobo");
    vazia.classList.add("vazia")
    div_lobo.classList.add("div-lobo")
    img_lobo.classList.add("img-lobo");
    conteudo_lobo.classList.add("conteudo-lobo")
    separador_lobo.classList.add("separador-lobo")
    header_lobo.classList.add("header-lobo")
    nome_lobo.classList.add("nome-lobo")
    link_lobo.setAttribute("href", "../Show Lobo/index.html")
    link_lobo.setAttribute("href", `../Show Lobo/index.html?id=${id}`)
    btn_header_lobo.classList.add("btn-header-lobo")
    idade_lobo.classList.add("idade-lobo")
    descricao_lobo.classList.add("descricao-lobo")
    
    btn_header_lobo.innerText = "Adotar"
    nome_lobo.innerText = nome;
    idade_lobo.innerHTML = "Idade: " + idade + " anos";
    pDescricao.innerText = descricao;
    img_lobo.src = foto;

    header_lobo.append(nome_lobo)
    header_lobo.append(link_lobo)
    link_lobo.append(btn_header_lobo)
    separador_lobo.append(header_lobo)
    separador_lobo.append(idade_lobo)
    descricao_lobo.append(pDescricao)
    conteudo_lobo.append(separador_lobo)
    conteudo_lobo.append(descricao_lobo)
    lobo.append(vazia)
    div_lobo.append(img_lobo)
    lobo.append(div_lobo)
    lobo.append(conteudo_lobo)

    let lobos = document.querySelector(".lobos")
    lobos.append(lobo)

    inverter = false
    
}


/************ LOBO PARA O OUTRO LADO ***************/ 


function createLobo2(nome, idade, descricao, foto, id) {

    let lobo = document.createElement("div");

    let vazia = document.createElement("vazia")
    let div_lobo = document.createElement("div")
    let img_lobo = document.createElement("img");
    let conteudo_lobo = document.createElement("div");

        let separador_lobo = document.createElement("div");
            let header_lobo = document.createElement("div");
            let link_lobo = document.createElement("a")
            let btn_header_lobo = document.createElement("button")
            let nome_lobo  = document.createElement("h3");
            let idade_lobo = document.createElement("h5");
        
        let descricao_lobo = document.createElement("div");
            let pDescricao = document.createElement("p");
    
    lobo.classList.add("lobo");
    lobo.setAttribute("id", "lobo2");
    vazia.classList.add("vazia")
    vazia.setAttribute("id", "vazia2")
    div_lobo.classList.add("div-lobo")
    img_lobo.classList.add("img-lobo");
    conteudo_lobo.classList.add("conteudo-lobo")
    separador_lobo.classList.add("separador-lobo")
    separador_lobo.setAttribute("id","separador-lobo2")
    header_lobo.classList.add("header-lobo")
    header_lobo.setAttribute("id", "header-lobo2")
    nome_lobo.classList.add("nome-lobo")
    link_lobo.setAttribute("href", "../Show Lobo/index.html")
    link_lobo.setAttribute("href", `../Show Lobo/index.html?id=${id}`)
    btn_header_lobo.classList.add("btn-header-lobo")
    idade_lobo.classList.add("idade-lobo")
    descricao_lobo.classList.add("descricao-lobo")
    
    btn_header_lobo.innerText = "Adotar"
    nome_lobo.innerText = nome;
    idade_lobo.innerHTML = "Idade: " + idade + " anos";
    pDescricao.innerText = descricao;
    img_lobo.src = foto;

    header_lobo.append(link_lobo)
    header_lobo.append(nome_lobo)
    link_lobo.append(btn_header_lobo)
    separador_lobo.append(header_lobo)
    separador_lobo.append(idade_lobo)
    descricao_lobo.append(pDescricao)
    conteudo_lobo.append(separador_lobo)
    conteudo_lobo.append(descricao_lobo)
    lobo.append(vazia)
    div_lobo.append(img_lobo)
    lobo.append(div_lobo)
    lobo.append(conteudo_lobo)

    let lobos = document.querySelector(".lobos")
    lobos.append(lobo)

    inverter = true
    
}

function getLobos(){

    const fetchConfig = {
        "method": "GET"
    }

    var i = 0;

    fetch(urlAPI, fetchConfig)
        .then((resposta) =>{
            resposta.json()
            .then((resposta) => {
                resposta.map((lobo) => {
                    if (inverter){
                        createLobo(lobo.nome, lobo.idade, lobo.descricao, lobo.imagem, i);
                        i++;
                    }
                    else {
                        createLobo2(lobo.nome, lobo.idade, lobo.descricao, lobo.imagem, i);
                        i++;
                    }
                })
            })
            .catch((error) => {
                console.log("erro 2")
                console.log(error);
            })
        })
        .catch((error)=>{
            console.log("error 1")
            console.log(error)
        })
        
    }
    

    getLobos();


/************ FILTRO DE PESQUISA POR NOME - INCOMPLETO***************/


    let textBox = document.querySelector("#textbox");
    textBox.oninput = filterUsers;
    
    function filterUsers(){
    
        const liElements = document.querySelectorAll("h3");
        const lobo = document.querySelectorAll(".lobo")
        for (let li of liElements) {  
            const currentName = li.innerHTML.toLowerCase();
            const searchText = this.value.toLowerCase();
            if (!currentName.includes(searchText)){
                li.setAttribute("hidden", true)
                lobo.setAttribute("hidden", true)
            }
            else {
                li.removeAttribute("hidden");
                lobo.removeAttribute("hidden");
            }
        }
    }
    


/************ CRIAR LOBOS ADOTADOS E DAR GET - INCOMPLETO ***************/

function createLoboAdopted(nome, idade, descricao, foto) {
    
    let lobo = document.createElement("div");
    
    let vazia = document.createElement("vazia")
    let div_lobo = document.createElement("div")
    let img_lobo = document.createElement("img");
    let conteudo_lobo = document.createElement("div");
    
    let separador_lobo = document.createElement("div");
    let header_lobo = document.createElement("div");
                let nome_lobo  = document.createElement("h3");
                let btn_header_lobo = document.createElement("button")
            let idade_lobo = document.createElement("h5");
        
        let descricao_lobo = document.createElement("div");
            let pDescricao = document.createElement("p");
    
            lobo.classList.add("lobo");
    vazia.classList.add("vazia")
    div_lobo.classList.add("div-lobo")
    img_lobo.classList.add("img-lobo");
    conteudo_lobo.classList.add("conteudo-lobo")
    separador_lobo.classList.add("separador-lobo")
    header_lobo.classList.add("header-lobo")
    nome_lobo.classList.add("nome-lobo")
    btn_header_lobo.classList.add("btn-header-lobo")
    idade_lobo.classList.add("idade-lobo")
    descricao_lobo.classList.add("descricao-lobo")
    
    btn_header_lobo.innerText = "Adotado"
    nome_lobo.innerText = nome;
    idade_lobo.innerHTML = "Idade: " + idade + " anos";
    pDescricao.innerText = descricao;
    img_lobo.src = foto;

    header_lobo.append(nome_lobo)
    header_lobo.append(btn_header_lobo)
    separador_lobo.append(header_lobo)
    separador_lobo.append(idade_lobo)
    descricao_lobo.append(pDescricao)
    conteudo_lobo.append(separador_lobo)
    conteudo_lobo.append(descricao_lobo)
    lobo.append(vazia)
    div_lobo.append(img_lobo)
    lobo.append(div_lobo)
    lobo.append(conteudo_lobo)

    let lobos = document.querySelector(".lobos")
    lobos.append(lobo)
    
}

function getLobosAdopted(){

    const fetchConfig = {
        "method": "GET"
    }

    fetch(urlAPI, fetchConfig)
        .then((resposta) =>{
            resposta.json()
            .then((resposta) => {
                const adoptedWolves = resposta.filter((lobo) => {
                    lobo.adopted = true
                })
                adoptedWolves.map(wolf => {
                    createLoboAdopted(wolf.nome, wolf.idade, wolf.descricao, wolf.imagem);
                })
            })
            .catch((error) => {
                console.log("erro 2")
                console.log(error);
            })
        })
        .catch((error)=>{
            console.log("error 1")
            console.log(error)
        })
}


