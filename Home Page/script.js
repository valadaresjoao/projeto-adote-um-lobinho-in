const urlAPI = "../lobinhos.json";

function createLobo(nome, idade, descricao, foto) {

    let lobo = document.createElement("div");

    let vazia = document.createElement("vazia")
    let div_lobo = document.createElement("div")
    let img_lobo = document.createElement("img");
    let conteudo_lobo = document.createElement("div");

        let separador_lobo = document.createElement("div");
            let header_lobo = document.createElement("div");
                let nome_lobo  = document.createElement("h3");
            let idade_lobo = document.createElement("h5");
        
        let descricao_lobo = document.createElement("div");
            let pDescricao = document.createElement("p");
    
    lobo.classList.add("lobo");
    vazia.classList.add("vazia")
    div_lobo.classList.add("div-lobo")
    img_lobo.classList.add("img-lobo");
    conteudo_lobo.classList.add("conteudo-lobo")
    separador_lobo.classList.add("separador-lobo")
    header_lobo.classList.add("header-lobo")
    nome_lobo.classList.add("nome-lobo")
    idade_lobo.classList.add("idade-lobo")
    descricao_lobo.classList.add("descricao-lobo")
    
    nome_lobo.innerText = nome;
    idade_lobo.innerHTML = "Idade: " + idade + " anos";
    pDescricao.innerText = descricao;
    img_lobo.src = foto;

    header_lobo.append(nome_lobo)
    separador_lobo.append(header_lobo)
    separador_lobo.append(idade_lobo)
    descricao_lobo.append(pDescricao)
    conteudo_lobo.append(separador_lobo)
    conteudo_lobo.append(descricao_lobo)
    lobo.append(vazia)
    div_lobo.append(img_lobo)
    lobo.append(div_lobo)
    lobo.append(conteudo_lobo)

    let lobos = document.querySelector(".lobos")
    lobos.append(lobo)
    
}

function createLobo2(nome, idade, descricao, foto) {

    let lobo = document.createElement("div");

    let vazia = document.createElement("vazia")
    let div_lobo = document.createElement("div")
    let img_lobo = document.createElement("img");
    let conteudo_lobo = document.createElement("div");

        let separador_lobo = document.createElement("div");
            let header_lobo = document.createElement("div");
                let nome_lobo  = document.createElement("h3");
            let idade_lobo = document.createElement("h5");
        
        let descricao_lobo = document.createElement("div");
            let pDescricao = document.createElement("p");
    
    lobo.classList.add("lobo");
    lobo.setAttribute("id", "lobo2");
    vazia.classList.add("vazia")
    vazia.setAttribute("id", "vazia2")
    div_lobo.classList.add("div-lobo")
    img_lobo.classList.add("img-lobo");
    conteudo_lobo.classList.add("conteudo-lobo")
    separador_lobo.classList.add("separador-lobo")
    separador_lobo.setAttribute("id","separador-lobo2")
    header_lobo.classList.add("header-lobo")
    nome_lobo.classList.add("nome-lobo")
    idade_lobo.classList.add("idade-lobo")
    descricao_lobo.classList.add("descricao-lobo")
    
    nome_lobo.innerText = nome;
    idade_lobo.innerHTML = "Idade: " + idade + " anos";
    pDescricao.innerText = descricao;
    img_lobo.src = foto;

    header_lobo.append(nome_lobo)
    separador_lobo.append(header_lobo)
    separador_lobo.append(idade_lobo)
    descricao_lobo.append(pDescricao)
    conteudo_lobo.append(separador_lobo)
    conteudo_lobo.append(descricao_lobo)
    lobo.append(vazia)
    div_lobo.append(img_lobo)
    lobo.append(div_lobo)
    lobo.append(conteudo_lobo)

    let lobos = document.querySelector(".lobos")
    lobos.append(lobo)
    
}


function getLobos(){

    var index = Math.floor(Math.random() * 1000)
    var index2 = Math.floor(Math.random() * 1000)

    const fetchConfig = {
        "method": "GET"
    }

    fetch(urlAPI, fetchConfig)
        .then((resposta) =>{
            resposta.json()
            .then((resposta) => {
                    createLobo(resposta[index].nome, resposta[index].idade, resposta[index].descricao, resposta[index].imagem);
                    createLobo2(resposta[index2].nome, resposta[index2].idade, resposta[index2].descricao, resposta[index2].imagem);
                
            })
            .catch((error) => {
                console.log("erro 2")
                console.log(error);
            })
        })
        .catch((error)=>{
            console.log("error 1")
            console.log(error)
        })

}

getLobos()