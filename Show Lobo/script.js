const urlAPI = "../lobinhos.json";

function createLobo(nome, descricao, foto) {

    let nome_lobo  = document.createElement("h1");
    
    let lobo = document.createElement("div");
        let vazia = document.createElement("vazia")
        let div_lobo = document.createElement("div")
            let img_lobo = document.createElement("img");
            let botoes = document.createElement("div")
            let link_adotar = document.createElement("a")
                let adotar = document.createElement("button")
                let excluir = document.createElement("button")
        let conteudo_lobo = document.createElement("div");        
                let pDescricao = document.createElement("p");
    
    nome_lobo.classList.add("nome-lobo")
    lobo.classList.add("lobo");
    vazia.classList.add("vazia")
    div_lobo.classList.add("div-lobo")
    img_lobo.classList.add("img-lobo");
    botoes.classList.add("botoes")
    link_adotar.setAttribute("href", "../Adotar Lobo/index.html")
    adotar.classList.add("adotar")
    excluir.classList.add("excluir")
    conteudo_lobo.classList.add("conteudo-lobo")
    
    nome_lobo.innerText = nome;
    pDescricao.innerText = descricao;
    adotar.innerText = "ADOTAR"
    excluir.innerText = "EXCLUIR"
    img_lobo.src = foto;

    conteudo_lobo.append(pDescricao);
    link_adotar.append(adotar)
    botoes.append(link_adotar);
    botoes.append(excluir);
    div_lobo.append(img_lobo);
    div_lobo.append(botoes);
    lobo.append(vazia);
    lobo.append(div_lobo);
    lobo.append(conteudo_lobo);


    let lobos = document.querySelector(".lobos")
    lobos.append(nome_lobo)
    lobos.append(lobo)
}


function getLobos(id){

    const fetchConfig = {
        "method": "GET"
    }

    fetch(urlAPI, fetchConfig)
        .then((resposta) =>{
            resposta.json()
            .then((resposta) => {
                    createLobo(resposta[id].nome, resposta[id].descricao, resposta[id].imagem);                
            })
            .catch((error) => {
                console.log("erro 2")
                console.log(error);
            })
        })
        .catch((error)=>{
            console.log("error 1")
            console.log(error)
        })

}

let url = new URLSearchParams(window.location.search);
let id = url.get("id");

getLobos(id);