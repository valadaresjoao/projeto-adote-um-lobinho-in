const urlAPI = "../lobinhos.json";

async function sendLobo() {

    let nome = document.querySelector("#input-nome-lobo").value;
    let idade = document.querySelector("#anos").value;
    parseInt(idade);
    let descricao = document.querySelector("#descricao").value;
    let imagem = document.querySelector("#imagem").value;


    if (nome.length > 60 || nome.length < 4 || nome === ""){
        alert("O nome do Lobo deve ter entre 4 e 60 caracteres")
    }
    else if (idade < 0 || idade > 100 || idade == NaN || idade == "") {
        alert("O lobo deve ter entre 0 e 100 anos")
    }
    else if (descricao === "" || descricao.length < 10 || descricao.length > 255) {
        alert("Você deve enviar uma descrição entre 10 e 255 caracteres.")
    }
    else if (imagem === "") {
        alert("Você deve enviar um link de imagem.")
    }
    else {


        let fetchBody = {
            "nome": nome,
            "idade": idade,
            "descricao": descricao,
            "imagem": imagem
        }

        const fetchConfig = {
            "method" : "POST",
            "body" : JSON.stringify(fetchBody),
            "headers" : {
                "Content-Type" : "application/json"
            }
        }

        await fetch(urlAPI, fetchConfig)
            .then((resposta) =>{
                resposta.json()
                    .then((resposta) =>{
                        console.log(resposta);
                        alert("O seu lobo foi adicionado!")
                    })
                    .catch((error) =>{
                        console.log(error);
                    })
            })
            .catch((error) =>{
                console.log(error);
            })        
    }

}

let btn_send = document.querySelector(".btn-submit");
btn_send.addEventListener("click", () => {sendLobo()})



